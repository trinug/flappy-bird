﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeSpawner : MonoBehaviour {
	
	[SerializeField] private Bird bird;
	[SerializeField] private Pipe pipeUp,pipeDown;
	[SerializeField] private Points point;


	[SerializeField] private float spawnInterval = 1;
	[SerializeField] public float holeSize = 1f; 
	[SerializeField] private float maxMinOffset = 1;

	private Coroutine CR_Spawn;

	// Use this for initialization
	void Start () {
		StartSpawn();
	}

	// Update is called once per frame
	void Update () {

	}
	void StartSpawn() {
		if (CR_Spawn == null) {
			CR_Spawn = StartCoroutine (IeSpawn ());
		}
	}

	void StopSpawn() {
		if (CR_Spawn != null) {
			StopCoroutine (CR_Spawn);
		}
	}

	void SpawnPipe() {
		Pipe newPipeUp   =  Instantiate(pipeUp,transform.position,Quaternion.Euler(0,0,180));
		newPipeUp.gameObject.SetActive(true);
		Pipe newPipeDown =  Instantiate(pipeDown,transform.position,Quaternion.identity);
		newPipeDown.gameObject.SetActive(true);
		Points newPoint = Instantiate(point, transform.position,Quaternion.identity);
		newPoint.gameObject.SetActive(true);

		//lubang
		newPipeUp.transform.position += Vector3.up * (holeSize / 2);
		newPipeDown.transform.position += Vector3.down * (holeSize / 2);
		newPoint.SetSize(holeSize);

		//kurva
		float y = maxMinOffset * Mathf.Sin(Time.time);
		newPipeUp.transform.position += Vector3.up * y;
		newPipeDown.transform.position += Vector3.up * y;
		newPoint.transform.position += Vector3.up * y;

	}

	IEnumerator IeSpawn() {
		while (true) {
			if (bird.IsDead ()) {
				StopSpawn ();
			}
			SpawnPipe ();
			yield return new WaitForSeconds (spawnInterval);
		}
	}
}
